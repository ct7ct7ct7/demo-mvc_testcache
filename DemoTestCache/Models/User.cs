﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoTestCache.Models {
    public class User {

        public DateTime LastUpdatedAt { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        override
        public int GetHashCode() {
            return LastUpdatedAt.GetHashCode() + Age.GetHashCode() + Name.GetHashCode();
        }
    }
}