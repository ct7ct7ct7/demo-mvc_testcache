﻿using DemoTestCache.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoTestCache.Helpers {
    public class TestCacheHelper {

        public static string convertToEtag(int hashCode) {
            return "\"" + hashCode + "\"";
        }

        public static User getUser() {
            User user = new User() {
                Age = 18,
                Name = "Anson",
                LastUpdatedAt = new DateTime(2000, 1, 1),
            };
            return user;
        }
    }
}