﻿using DemoTestCache.Helpers;
using DemoTestCache.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DemoTestCache.Controllers.Api {
    public class UserController : ApiController {
        public HttpResponseMessage Get() {
            User user = TestCacheHelper.getUser();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
            response.Headers.ETag = new EntityTagHeaderValue(TestCacheHelper.convertToEtag(user.GetHashCode()));
            response.Content.Headers.LastModified = user.LastUpdatedAt.ToUniversalTime(); //GMT
            response.Headers.CacheControl = new CacheControlHeaderValue() {
                Private = true,
                MaxAge = TimeSpan.FromSeconds(10)
            };

            var modifiedSince = Request.Headers.IfModifiedSince;
            var eTag = Request.Headers.IfNoneMatch.FirstOrDefault();
            if (modifiedSince != null && modifiedSince.Value.ToUniversalTime().Equals(user.LastUpdatedAt)) {
                response.StatusCode = HttpStatusCode.NotModified; //304
            }
            if (eTag != null && eTag.ToString().Equals(TestCacheHelper.convertToEtag(user.GetHashCode()))) {
                response.StatusCode = HttpStatusCode.NotModified; //304
            }
            return response; 
        }
    }
}