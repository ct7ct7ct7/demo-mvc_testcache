﻿using DemoTestCache.Helpers;
using DemoTestCache.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DemoTestCache.Controllers.Api {
    public class UserOnlyMaxAgeController : ApiController {
        public HttpResponseMessage Get() {
            User user = TestCacheHelper.getUser();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
            response.Headers.CacheControl = new CacheControlHeaderValue() {
                Private = true,
                MaxAge = TimeSpan.FromSeconds(10)
            };
            return response; 
        }
    }
}